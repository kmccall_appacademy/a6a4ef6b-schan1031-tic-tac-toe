require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_reader :board, :player_one, :player_two, :current_player

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @current_player = player_one
    @board = Board.new
  end

  def play_turn

    current_player.display
    move = current_player.get_move
    board.place_mark(move, current_player.mark)
    switch_players!
  end

  def switch_players!
    if current_player == player_one
      current_player = player_two
    else
      current_player = player_one
    end
  end

end
