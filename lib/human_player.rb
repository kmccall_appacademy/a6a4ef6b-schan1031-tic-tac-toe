class HumanPlayer
  attr_reader :name
  def initialize(name)
    @name = name
  end

  def get_move
    print 'Enter where you want to place mark: '
    input = gets.chomp
    return input.split(', ').map(&:to_i)
  end

  def display(board)
    puts board.grid
  end
end
