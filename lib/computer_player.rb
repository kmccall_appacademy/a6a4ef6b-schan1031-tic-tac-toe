class ComputerPlayer
  attr_reader :board
  def initialize(name)
    @name = name
    @mark
    @board
  end

  def display(board)
    @board = board
  end

  def mark= mark
    @mark = mark
  end

  def get_move
    return [2, 0] if board.grid[0][0] == board.grid[1][0]
  end
end
