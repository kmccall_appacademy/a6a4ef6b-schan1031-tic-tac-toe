class Board
  attr_reader :grid
  def initialize(grid = [[nil, nil, nil], [nil, nil, nil],[nil, nil, nil]])
    @grid = grid
  end

  def place_mark(loc, mark)
    if !grid[loc[0]][loc[1]]
      grid[loc[0]][loc[1]] = mark
    else
      puts 'Invalid Location'
    end
  end

  def empty?(loc)
    return true if !grid[loc[0]][loc[1]]
    false
  end

  def winner

    return grid[0][0] if grid[0][0] == grid[1][1] && grid[1][1] == grid[2][2] && grid[0][0] != nil
    return grid[2][0] if grid[2][0] == grid[1][1] && grid[1][1] == grid[0][2] && grid[2][0] != nil

    # Rows
    return grid[0][0] if grid[0][0] == grid[0][1] && grid[0][1] == grid[0][2] && grid[0][0] != nil
    return grid[1][0] if grid[1][0] == grid[1][1] && grid[1][1] == grid[1][2] && grid[1][0] != nil
    return grid[2][0] if grid[2][0] == grid[2][1] && grid[2][1] == grid[2][2] && grid[2][0] != nil

    # Columns
    return grid[0][0] if grid[0][0] == grid[1][0] && grid[1][0] == grid[2][0] && grid[0][0] != nil
    return grid[0][1] if grid[0][1] == grid[1][1] && grid[1][1] == grid[2][1] && grid[0][1] != nil
    return grid[0][2] if grid[0][2] == grid[1][2] && grid[1][2] == grid[2][2] && grid[0][2] != nil
  end

  def over?
    return true if winner
    return true if !grid[0].include?(nil) && !grid[1].include?(nil) && !grid[2].include?(nil)
  end

end
